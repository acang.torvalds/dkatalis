package com.company.controller;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

/**
 * @author hasan
 */
public class ControllerSession {
    private final Cache cache;

    public ControllerSession(Cache cacheCookies) {
        this.cache = cacheCookies;
    }

    public JSONObject checkExistCookie(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        if (this.cache.isKeyInCache(jsonReq.get("sessId").toString())) {
            Element elemCache = this.cache.get(jsonReq.get("sessId").toString());
            if (elemCache != null) {
                if (this.cache.isExpired(elemCache)) {
                    //removeCookie(jsonReq);
                    jsonResp.put("result", "false");
                    return jsonResp;
                } else {
                    jsonResp = this.checkExpireCookie(jsonReq);
                    if (jsonResp.get("result").toString().equals("true")) {
                        jsonResp.put("result", "false");
                    } else {
                        jsonResp.put("result", "true");
                    }
                    return jsonResp;
                }
            } else {
                //this.removeCookie(jsonReq);
                jsonResp.put("result", "false");
                return jsonResp;
            }
        } else {
            jsonResp.put("result", "false");
            return jsonResp;
        }
    }

    public JSONObject checkExpireCookie(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        Element elemCache = this.cache.get(jsonReq.get("sessId").toString());
        JSONObject jSONObject = new JSONObject();
        String cookieStore = "";
        String expireDate = "";

        //System.out.println("expireDate :" + expireDate);
        Date d1 = null;
        Date d2 = null;

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HHmmss");
        Date date = new Date();

        long totalDiffSecond = 0;
        try {
            jSONObject = (JSONObject) new JSONParser().parse(String.valueOf(elemCache.getObjectValue()));
            cookieStore = (String) jSONObject.get("cookie");
            expireDate = (String) jSONObject.get("expireDate");

            String dateStart = format.format(date);
            String dateStop = expireDate;

            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            long secondDay = diffDays * 1440 * 60;
            long secondHour = diffHours * 60 * 60;
            long secondMinute = diffMinutes * 60;
            long secondSec = diffSeconds;
            totalDiffSecond = secondDay + secondHour + secondMinute + secondSec;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (totalDiffSecond <= 0) {
            //this.removeCookie(jsonReq);
            jsonResp.put("result", "true");
            return jsonResp;
        } else {
            jsonResp.put("result", "false");
            return jsonResp;
        }
    }

    public JSONObject setCookie(JSONObject jsonReq) {
        String sessionId = jsonReq.get("sessId").toString();

        //save cookie to cache
        Calendar calendar = Calendar.getInstance();
        int sessionTtl = Integer.parseInt("86400");
        if (jsonReq.containsKey("sessionTtl")) {
            sessionTtl = Integer.parseInt(jsonReq.get("sessionTtl").toString());
        }
        calendar.add(Calendar.SECOND, sessionTtl);
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HHmmss");
        String formatted = format.format(calendar.getTime());
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("cookie", sessionId);
        jSONObject.put("expireDate", formatted);
        jSONObject.put("maxAge", String.valueOf(sessionTtl));

        this.cache.put(new Element(sessionId, jSONObject.toString()));
        return jSONObject;
    }

    public JSONObject renewCookie(JSONObject jsonReq) {
        String sessionId = jsonReq.get("sessId").toString();
        //save cookie to cache
        Calendar calendar = Calendar.getInstance();
        int sessionTtl = Integer.parseInt("86400");
        if (jsonReq.containsKey("sessionTtl")) {
            sessionTtl = Integer.parseInt(jsonReq.get("sessionTtl").toString());
        }
        calendar.add(Calendar.SECOND, sessionTtl);
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HHmmss");
        String formatted = format.format(calendar.getTime());
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("cookie", sessionId);
        jSONObject.put("expireDate", formatted);
        jSONObject.put("maxAge", String.valueOf(sessionTtl));
        //copy others data from old cookie
        if (!sessionId.equals("")) {
            JSONObject jsonResp = this.checkExistCookie(jsonReq);
            if (jsonResp.get("result").toString().equals("true")) {
                Element elemCache = this.cache.get(sessionId);
                try {
                    JSONObject jSONObjectOld = (JSONObject) new JSONParser().parse(String.valueOf(elemCache.getObjectValue()));
                    for (Iterator iterator = jSONObjectOld.keySet().iterator(); iterator.hasNext(); ) {
                        String key = (String) iterator.next();
                        //System.out.println(this.jSONObject.get(key));
                        if (!key.equals("cookie") && !key.equals("expireDate") && !key.equals("maxAge")) {
                            jSONObject.put(key, jSONObjectOld.get(key));
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                jSONObject = new JSONObject();
                jSONObject.put("result", "false");
            }
        }
        this.cache.put(new Element(sessionId, jSONObject.toString()));
        return jSONObject;
    }

    public JSONObject getDataCookie(JSONObject jsonReq) {
        JSONObject jsonResp = this.checkExistCookie(jsonReq);
        JSONObject jSONObject = new JSONObject();
        try {
            if (jsonResp.get("result").toString().equals("true")) {
                Element elemCache = this.cache.get(jsonReq.get("sessId"));
                jSONObject = (JSONObject) new JSONParser().parse(String.valueOf(elemCache.getObjectValue()));
            } else {
                jSONObject = jsonResp;
                //this.removeCookie(jsonReq);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return jSONObject;
    }

    public JSONObject addDataCookie(JSONObject jsonReq) {
        JSONObject jsonResp = this.checkExistCookie(jsonReq);
        if (jsonResp.get("result").toString().equals("true")) {
            Element elemCache = this.cache.get(jsonReq.get("sessId").toString());
            try {
                JSONObject jSONObject = (JSONObject) new JSONParser().parse(String.valueOf(elemCache.getObjectValue()));
                jSONObject.put(jsonReq.get("key").toString(), jsonReq.get("value").toString());
                this.cache.put(new Element(jsonReq.get("sessId").toString(), jSONObject));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //this.removeCookie(jsonReq);
        }
        return this.getDataCookie(jsonReq);
    }

    public void removeCookie(final JSONObject jsonReq) {
        cache.remove(jsonReq.get("sessId").toString());
    }

    public void initData() {
        //-------------init alice----------------------
        JSONObject jObjSession = new JSONObject();
        jObjSession.put("sessId", "allice");
        this.setCookie(jObjSession);

        JSONObject jObjAccount = new JSONObject();
        jObjAccount.put("sessId", "allice");
        jObjAccount.put("key", "username");
        jObjAccount.put("value", "alice");
        this.addDataCookie(jObjAccount);

        jObjAccount = new JSONObject();
        jObjAccount.put("sessId", "allice");
        jObjAccount.put("key", "balance");
        jObjAccount.put("value", "30");
        this.addDataCookie(jObjAccount);

        jObjAccount = new JSONObject();
        jObjAccount.put("sessId", "allice");
        jObjAccount.put("key", "owed_to");
        JSONArray jArrOwed = new JSONArray();
        JSONObject jObjOwed = new JSONObject();
        jArrOwed.add(jObjOwed);
        jObjAccount.put("value", jArrOwed);
        this.addDataCookie(jObjAccount);

        jObjAccount = new JSONObject();
        jObjAccount.put("sessId", "allice");
        jObjAccount.put("key", "owed_from");
        jArrOwed = new JSONArray();
        jObjOwed = new JSONObject();
        jArrOwed.add(jObjOwed);
        jObjAccount.put("value", jArrOwed);
        this.addDataCookie(jObjAccount);

        //---------------init bob------------------------
        jObjSession = new JSONObject();
        jObjSession.put("sessId", "bob");
        this.setCookie(jObjSession);

        jObjAccount = new JSONObject();
        jObjAccount.put("sessId", "bob");
        jObjAccount.put("key", "username");
        jObjAccount.put("value", "bob");
        this.addDataCookie(jObjAccount);

        jObjAccount = new JSONObject();
        jObjAccount.put("sessId", "bob");
        jObjAccount.put("key", "balance");
        jObjAccount.put("value", "0");
        this.addDataCookie(jObjAccount);

        jObjAccount = new JSONObject();
        jObjAccount.put("sessId", "bob");
        jObjAccount.put("key", "owed_to");
        jArrOwed = new JSONArray();
        jObjOwed = new JSONObject();
        jArrOwed.add(jObjOwed);
        jObjAccount.put("value", jArrOwed);
        this.addDataCookie(jObjAccount);

        jObjAccount = new JSONObject();
        jObjAccount.put("sessId", "bob");
        jObjAccount.put("key", "owed_from");
        jArrOwed = new JSONArray();
        jObjOwed = new JSONObject();
        jArrOwed.add(jObjOwed);
        jObjAccount.put("value", jArrOwed);
        this.addDataCookie(jObjAccount);

        //----------------init user collection--------------------
        jObjSession = new JSONObject();
        jObjSession.put("sessId", "userList");
        this.setCookie(jObjSession);

        JSONArray jArrUserList = new JSONArray();
        jArrUserList.add("allice");
        jArrUserList.add("bob");
        jObjAccount = new JSONObject();
        jObjAccount.put("sessId", "userList");
        jObjAccount.put("key", "userList");
        jObjAccount.put("value", jArrUserList);
        this.addDataCookie(jObjAccount);
    }
}
