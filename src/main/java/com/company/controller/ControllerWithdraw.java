package com.company.controller;

import com.company.entity.SessionActiveEntity;
import com.company.implement.ImpleDeposit;
import com.company.implement.ImpleWithdraw;
import com.company.interfc.InterfcDeposit;
import com.company.interfc.InterfcWithdraw;
import net.sf.ehcache.Cache;
import org.json.simple.JSONObject;

public class ControllerWithdraw {

    private final Cache cache;
    private final ControllerSession controllerSession;
    private final InterfcWithdraw service;
    private final SessionActiveEntity sessionActiveEntity;

    public ControllerWithdraw(Cache cache, SessionActiveEntity sessionActiveEntity) {
        this.cache = cache;
        this.sessionActiveEntity = sessionActiveEntity;
        this.service = new ImpleWithdraw(cache);
        this.controllerSession = new ControllerSession(cache);
    }

    public JSONObject handleWithdraw(String deposit) {
        JSONObject jObjResp = new JSONObject();
        try {
            Double depositDbl = Double.parseDouble(deposit);
            if (depositDbl == Double.parseDouble("0")) {
                jObjResp.put("rc", "01");
                jObjResp.put("message", "number must be greater than zero!");
                return jObjResp;
            } else if (depositDbl < Double.parseDouble("0")) {
                jObjResp.put("rc", "01");
                jObjResp.put("message", "number must be greater than zero!");
                return jObjResp;
            }
            jObjResp = this.service.debitBalance(sessionActiveEntity, deposit);
        } catch (Exception ex){
            jObjResp.put("rc", "01");
            jObjResp.put("message", "something wrong, please input numeric only!");
        }
        return jObjResp;
    }
}
