package com.company.controller;

import com.company.implement.ImpleLogin;
import com.company.interfc.InterfcLogin;
import net.sf.ehcache.Cache;
import org.json.simple.JSONObject;

import java.util.Scanner;

public class ControllerLogin {

    private final Cache cache;
    private final ControllerSession controllerSession;
    private final InterfcLogin service;

    public ControllerLogin(Cache cache) {
        this.cache = cache;
        this.service =  new ImpleLogin(cache);
        this.controllerSession = new ControllerSession(cache);
    }

    public JSONObject handleLogin(String userName) {
        JSONObject jObjSess = new JSONObject();
        jObjSess.put("sessId", userName);
        JSONObject jObjResp = this.service.getCookie(jObjSess);
        if (jObjResp.containsKey("cookie")) {
            jObjResp.put("rc", "00");
        } else {
            //init new user
            this.service.setCookie(userName);

            //get new data
            jObjSess.put("sessId", userName);
            jObjResp = this.service.getCookie(jObjSess);
            jObjResp.put("rc", "00");
        }
        return jObjResp;
    }
}
