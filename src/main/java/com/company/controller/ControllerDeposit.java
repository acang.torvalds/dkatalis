package com.company.controller;

import com.company.entity.SessionActiveEntity;
import com.company.implement.ImpleDeposit;
import com.company.implement.ImpleLogin;
import com.company.interfc.InterfcDeposit;
import com.company.interfc.InterfcLogin;
import net.sf.ehcache.Cache;
import org.json.simple.JSONObject;

public class ControllerDeposit {

    private final Cache cache;
    private final ControllerSession controllerSession;
    private final InterfcDeposit service;
    private final SessionActiveEntity sessionActiveEntity;

    public ControllerDeposit(Cache cache, SessionActiveEntity sessionActiveEntity) {
        this.cache = cache;
        this.sessionActiveEntity = sessionActiveEntity;
        this.service = new ImpleDeposit(cache);
        this.controllerSession = new ControllerSession(cache);
    }

    public JSONObject handleDeposit(String deposit) {
        JSONObject jObjResp = new JSONObject();
        try {
            Double depositDbl = Double.parseDouble(deposit);
            if (depositDbl == Double.parseDouble("0")) {
                jObjResp.put("rc", "01");
                jObjResp.put("message", "number must be greater than zero!");
                return jObjResp;
            } else if (depositDbl < Double.parseDouble("0")) {
                jObjResp.put("rc", "01");
                jObjResp.put("message", "number must be greater than zero!");
                return jObjResp;
            }
            jObjResp = this.service.addBalance(sessionActiveEntity, deposit);
            jObjResp.put("rc", "00");
        } catch (Exception ex) {
            jObjResp.put("rc", "01");
            jObjResp.put("message", "something wrong, please input numeric only!");
        }
        return jObjResp;
    }
}
