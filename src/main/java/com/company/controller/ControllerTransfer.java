package com.company.controller;

import com.company.entity.SessionActiveEntity;
import com.company.implement.ImpleDeposit;
import com.company.implement.ImpleTransfer;
import com.company.interfc.InterfcDeposit;
import com.company.interfc.InterfcTransfer;
import net.sf.ehcache.Cache;
import org.json.simple.JSONObject;

public class ControllerTransfer {

    private final Cache cache;
    private final ControllerSession controllerSession;
    private final InterfcTransfer service;
    private final SessionActiveEntity sessionActiveEntity;

    public ControllerTransfer(Cache cache, SessionActiveEntity sessionActiveEntity) {
        this.cache = cache;
        this.sessionActiveEntity = sessionActiveEntity;
        this.service = new ImpleTransfer(cache);
        this.controllerSession = new ControllerSession(cache);
    }

    public JSONObject handleTransfer(String[] lineInputArr) {
        JSONObject jObjResp = new JSONObject();
        try {
            String target = lineInputArr[1];
            Double amount = Double.parseDouble(lineInputArr[2]);
            if (amount == Double.parseDouble("0")) {
                jObjResp.put("rc", "01");
                jObjResp.put("message", "number must be greater than zero!");
                return jObjResp;
            } else if (amount < Double.parseDouble("0")) {
                jObjResp.put("rc", "01");
                jObjResp.put("message", "number must be greater than zero!");
                return jObjResp;
            }
            if(sessionActiveEntity.getActive().equals(target)){
                jObjResp.put("rc", "01");
                jObjResp.put("message", "you cant transfer to yourself!");
                return jObjResp;
            }
            jObjResp = this.service.transfer(sessionActiveEntity, target, amount);
        } catch (Exception ex) {
            jObjResp.put("rc", "01");
            jObjResp.put("message", "something wrong, please input with format \"transfer benerificiary amount\" or amount in numeric only!");
        }
        return jObjResp;
    }
}
