package com.company.entity;

public class SessionActiveEntity {

    private String userName = "";

    public void setActive(String username) {
        this.userName = username;
    }

    public String getActive() {
        return userName;
    }
}
