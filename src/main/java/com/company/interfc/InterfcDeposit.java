package com.company.interfc;

import com.company.entity.SessionActiveEntity;
import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcDeposit {

    JSONObject addBalance(SessionActiveEntity sessionActiveEntity, String deposit);
}
