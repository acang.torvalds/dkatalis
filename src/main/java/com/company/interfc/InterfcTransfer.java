package com.company.interfc;

import com.company.entity.SessionActiveEntity;
import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcTransfer {

    JSONObject transfer(SessionActiveEntity sessionActiveEntity, String target, Double amount);
}
