package com.company.interfc;

import com.company.entity.SessionActiveEntity;
import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcWithdraw {

    JSONObject debitBalance(SessionActiveEntity sessionActiveEntity, String deposit);
}
