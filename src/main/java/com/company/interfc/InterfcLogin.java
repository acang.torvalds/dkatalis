package com.company.interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcLogin {

    JSONObject setCookie(String sessId);

    JSONObject getCookie(JSONObject jSONObject);
}
