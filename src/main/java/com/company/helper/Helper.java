package com.company.helper;

import com.company.controller.ControllerSession;
import com.company.entity.SessionActiveEntity;
import net.sf.ehcache.Cache;
import org.json.simple.JSONObject;

public class Helper {

    private final Cache cache;
    private final ControllerSession controllerSession;

    public Helper(Cache cache){
        this.cache = cache;
        this.controllerSession = new ControllerSession(this.cache);
    }

    public JSONObject addDataSession(String sessId, String key, String value) {
        JSONObject jObjData = new JSONObject();
        jObjData.put("sessId", sessId);
        jObjData.put("key", key);
        jObjData.put("value", value);
        jObjData = this.controllerSession.addDataCookie(jObjData);
        return jObjData;
    }

    public JSONObject getDataSession(String sessId){
        JSONObject jObjData = new JSONObject();
        JSONObject jObjSess = new JSONObject();
        jObjSess.put("sessId", sessId);
        jObjData = this.controllerSession.getDataCookie(jObjSess);
        return jObjData;
    }
}
