package com.company;

import com.company.controller.*;
import com.company.entity.SessionActiveEntity;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.DiskStoreConfiguration;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            //--------------------------ehcache init for simulate database--------------------------------------
            // Already created a configuration object ...
            net.sf.ehcache.config.Configuration cacheConfiguration = new net.sf.ehcache.config.Configuration();
            //Create a CacheManager using custom configuration
            CacheManager cacheManager = CacheManager.create(cacheConfiguration);
            //--------------------------ehcache save the config entity------------------------
            Cache memoryOnlyCache = new Cache("DKATALIS", 1000, false, true, 0, 0);
            cacheManager.addCache(memoryOnlyCache);
            //--------------------------simulate list of account-------------------------
            ControllerSession controllerSession = new ControllerSession(memoryOnlyCache);
            controllerSession.initData();

            //------create new session active entity-----------------------
            SessionActiveEntity sessionActiveEntity = new SessionActiveEntity();

            //--------------run scanner input------------------------
            Scanner input = new Scanner(System.in);
            System.out.print("Enter command (type readme for list command): ");
            while (input.hasNext()) {
                String lineInput = input.nextLine();
                String[] lineInputArr = lineInput.split(" ");
                if (lineInputArr[0].equalsIgnoreCase("login")) {
                    //login handler router
                    ControllerLogin controllerLogin = new ControllerLogin(memoryOnlyCache);
                    JSONObject jObjLogin = controllerLogin.handleLogin(lineInputArr[1]);
                    if (jObjLogin.get("rc").equals("00")) {
                        sessionActiveEntity.setActive(lineInputArr[1]);
                        System.out.println("helo " + lineInputArr[1]);
                        System.out.println("your balance is : $" + jObjLogin.get("balance").toString());
                        JSONArray jArrOwedTo = (JSONArray) new JSONParser().parse(jObjLogin.get("owed_to").toString());
                        for(int i = 0; i < jArrOwedTo.size(); i++){
                            JSONObject jObjOwed = (JSONObject) jArrOwedTo.get(i);
                            if(jObjOwed.containsKey("target")) {
                                System.out.println("Owed $" + jObjOwed.get("amount") +" to " + jObjOwed.get("target"));
                            }
                        }
                        JSONArray jArrOwedFrom = (JSONArray) new JSONParser().parse(jObjLogin.get("owed_from").toString());
                        for(int i = 0; i < jArrOwedFrom.size(); i++){
                            JSONObject jObjOwed = (JSONObject) jArrOwedFrom.get(i);
                            if(jObjOwed.containsKey("target")) {
                                System.out.println("Owed $" + jObjOwed.get("amount") +" from " + jObjOwed.get("target"));
                            }
                        }
                        System.out.print("Enter command (type readme for list command): ");
                    } else {
                        //this line will not reach cause login failed will treat as new user
                        System.out.println("login fail for username " + lineInputArr[1]);
                        System.out.print("Enter command (type readme for list command): ");
                    }
                } else if (lineInputArr[0].equalsIgnoreCase("deposit")) {
                    //deposit handler router
                    if (!sessionActiveEntity.getActive().equals("")) {
                        ControllerDeposit controllerDeposit = new ControllerDeposit(memoryOnlyCache, sessionActiveEntity);
                        JSONObject jObjDeposit = controllerDeposit.handleDeposit(lineInputArr[1]);
                        if (jObjDeposit.get("rc").equals("00")) {
                            System.out.println("Yor balance is : $" + jObjDeposit.get("balance").toString());
                            JSONArray jArrOwedTo = (JSONArray) new JSONParser().parse(jObjDeposit.get("owed_to").toString());
                            for(int i = 0; i < jArrOwedTo.size(); i++){
                                JSONObject jObjOwed = (JSONObject) jArrOwedTo.get(i);
                                if(jObjOwed.containsKey("target")) {
                                    System.out.println("Owed $" + jObjOwed.get("amount") +" to " + jObjOwed.get("target"));
                                }
                            }
                            JSONArray jArrOwedFrom = (JSONArray) new JSONParser().parse(jObjDeposit.get("owed_from").toString());
                            for(int i = 0; i < jArrOwedFrom.size(); i++){
                                JSONObject jObjOwed = (JSONObject) jArrOwedFrom.get(i);
                                if(jObjOwed.containsKey("target")) {
                                    System.out.println("Owed $" + jObjOwed.get("amount") +" from " + jObjOwed.get("target"));
                                }
                            }
                            System.out.print("Enter command (type readme for list command): ");
                        } else {
                            System.out.println(jObjDeposit.get("message").toString());
                            System.out.print("Enter command (type readme for list command): ");
                        }
                    } else {
                        System.out.println("You should login first!");
                        System.out.print("Enter command (type readme for list command): ");
                    }
                } else if (lineInputArr[0].equalsIgnoreCase("withdraw")) {
                    //withdraw handler router
                    if (!sessionActiveEntity.getActive().equals("")) {
                        ControllerWithdraw controllerWithdraw = new ControllerWithdraw(memoryOnlyCache, sessionActiveEntity);
                        JSONObject jObjWithdraw = controllerWithdraw.handleWithdraw(lineInputArr[1]);
                        if (jObjWithdraw.get("rc").equals("00")) {
                            System.out.println("Yor balance is : $" + jObjWithdraw.get("balance").toString());
                            System.out.print("Enter command (type readme for list command): ");
                        } else {
                            System.out.println(jObjWithdraw.get("message").toString());
                            System.out.print("Enter command (type readme for list command): ");
                        }
                    } else {
                        System.out.println("You should login first!");
                        System.out.print("Enter command (type readme for list command): ");
                    }
                } else if (lineInputArr[0].equalsIgnoreCase("transfer")) {
                    //transfer handler router
                    if (!sessionActiveEntity.getActive().equals("")) {
                        ControllerTransfer controllerTransfer = new ControllerTransfer(memoryOnlyCache, sessionActiveEntity);
                        JSONObject jObjTransfer = controllerTransfer.handleTransfer(lineInputArr);
                        if (jObjTransfer.get("rc").equals("00")) {
                            System.out.println("Yor balance is : $" + jObjTransfer.get("balance").toString());
                            JSONArray jArrOwedTo = (JSONArray) new JSONParser().parse(jObjTransfer.get("owed_to").toString());
                            for(int i = 0; i < jArrOwedTo.size(); i++){
                                JSONObject jObjOwed = (JSONObject) jArrOwedTo.get(i);
                                if(jObjOwed.containsKey("target")) {
                                    System.out.println("Owed $" + jObjOwed.get("amount") +" to " + jObjOwed.get("target"));
                                }
                            }
                            JSONArray jArrOwedFrom = (JSONArray) new JSONParser().parse(jObjTransfer.get("owed_from").toString());
                            for(int i = 0; i < jArrOwedFrom.size(); i++){
                                JSONObject jObjOwed = (JSONObject) jArrOwedFrom.get(i);
                                if(jObjOwed.containsKey("target")) {
                                    System.out.println("Owed $" + jObjOwed.get("amount") +" from " + jObjOwed.get("target"));
                                }
                            }
                            System.out.print("Enter command (type readme for list command): ");
                        } else {
                            System.out.println(jObjTransfer.get("message").toString());
                            System.out.print("Enter command (type readme for list command): ");
                        }
                    } else {
                        System.out.println("You should login first!");
                        System.out.print("Enter command (type readme for list command): ");
                    }
                } else if (lineInputArr[0].equalsIgnoreCase("logout")) {
                    //logout handler router
                    System.out.println("Goodbye, " + sessionActiveEntity.getActive() + "!");
                    sessionActiveEntity.setActive("");
                    System.out.print("Enter command (type readme for list command): ");
                } else if (lineInputArr[0].equalsIgnoreCase("exit")) {
                    //exit system handler router
                    input.close();
                    System.exit(1);
                } else if (lineInputArr[0].equalsIgnoreCase("readme")) {
                    //readme handler router
                    System.out.println("please use command below");
                    System.out.println("1. login username (to login)");
                    System.out.println("2. deposit amount (to credit balace)");
                    System.out.println("3. withdraw amount (to withdraw balance)");
                    System.out.println("4. transfer username_beneficiary amount (to debit your amount and credit to beneficiary account)");
                    System.out.println("5. logout (to logout atm)");
                    System.out.print("Enter command (type readme for list command): ");
                } else {
                    //customer error handler router
                    System.out.println("Command not found");
                    System.out.print("Enter command (type readme for list command): ");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
