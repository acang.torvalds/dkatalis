/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.implement;

import com.company.controller.ControllerSession;
import com.company.entity.SessionActiveEntity;
import com.company.helper.Helper;
import com.company.interfc.InterfcDeposit;
import com.company.interfc.InterfcLogin;
import com.company.interfc.InterfcTransfer;
import net.sf.ehcache.Cache;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * @author Hasan
 */
public class ImpleTransfer implements InterfcTransfer {
    private final Cache cache;
    private final ControllerSession controllerSession;
    private final InterfcLogin service;
    private final Helper helper;

    public ImpleTransfer(Cache cacheMaster) {
        this.cache = cacheMaster;
        this.controllerSession = new ControllerSession(cache);
        this.service = new ImpleLogin(this.cache);
        this.helper = new Helper(this.cache);
    }

    @Override
    public JSONObject transfer(SessionActiveEntity sessionActiveEntity, String target, Double amount) {
        JSONObject jObjResp = new JSONObject();
        JSONObject jObjAccount = new JSONObject();
        JSONObject jObjAccountTarget = new JSONObject();
        JSONObject jObjOwedTo = new JSONObject();

        Double balanceSource = Double.parseDouble("0");
        Double balanceSourceBefore = Double.parseDouble("0");
        Double balanceBeneficiary = Double.parseDouble("0");
        try {
            //get data source account
            JSONObject jObjSess = new JSONObject();
            jObjSess.put("sessId", sessionActiveEntity.getActive());
            jObjAccount = this.controllerSession.getDataCookie(jObjSess);
            balanceSourceBefore = Double.parseDouble(jObjAccount.get("balance").toString());
//            if(balanceSourceBefore <= Double.parseDouble("0")){
//                jObjResp.put("rc", "01");
//                jObjResp.put("message", "Insufficient Balance!");
//                return jObjResp;
//            }
            balanceSource = balanceSourceBefore;
            balanceSource = balanceSource - amount;

            //get data beneficiary account
            jObjSess.put("sessId", target);
            jObjAccountTarget = this.controllerSession.getDataCookie(jObjSess);
            if (!jObjAccountTarget.containsKey("cookie")) {
                //create new user if not exist
                jObjAccountTarget = this.service.setCookie(target);
            }

            boolean foundTargetOwedTo = false;
            int foundIndexOwedTo = 0;
            double balanceOwedTo = Double.parseDouble("0");
            //condition for balance is insufficient
            if (balanceSource < Double.parseDouble("0")) {
                JSONArray jArrOwedTo = (JSONArray) new JSONParser().parse(jObjAccount.get("owed_to").toString());
                //block if have owed_from list has target account
                JSONArray jArrOwedFrom = (JSONArray) new JSONParser().parse(jObjAccount.get("owed_from").toString());
                boolean foundOwedFrom = false;
                for (int i = 0; i < jArrOwedFrom.size(); i++) {
                    JSONObject jObjOwedFrom = (JSONObject) jArrOwedFrom.get(i);
                    if(jObjOwedFrom.containsKey("target")){
                        if(jObjOwedFrom.get("target").toString().equals(target)){
                            foundOwedFrom = true;
                            break;
                        }
                    }
                }
                if(foundOwedFrom){
                    jObjResp.put("rc", "01");
                    jObjResp.put("message", "Account beneficiary have owed to you, so you can owed to beneficiary too!");
                    return jObjResp;
                }
                for (int i = 0; i < jArrOwedTo.size(); i++) {
                    jObjOwedTo = (JSONObject) jArrOwedTo.get(i);
                    if (jObjOwedTo.containsKey("target")) {
                        //searching if benef account has already on owed list
                        if (jObjOwedTo.get("target").toString().equals(target)) {
                            foundTargetOwedTo = true;
                            foundIndexOwedTo = i;
                            balanceOwedTo = Double.parseDouble(jObjOwedTo.get("amount").toString());
                            break;
                        }
                    }
                }
                if (foundTargetOwedTo) {
                    jArrOwedTo.remove(foundIndexOwedTo); //remove first to avoid redundancy list owed target
                }
//                else {
                    //add balance amount beneficiary
                    balanceBeneficiary = Double.parseDouble(jObjAccountTarget.get("balance").toString());
                    jObjAccountTarget = this.helper.addDataSession(target, "balance", String.valueOf(balanceBeneficiary + Math.abs(balanceSourceBefore)));

                    //add data owed_from
                    boolean foundTargetOwedFrom = false;
                    int foundIndexOwedFrom = 0;
                    double balanceOwedFrom = Double.parseDouble("0");
                    jObjAccountTarget.put("sessId", target);
                    jArrOwedFrom = (JSONArray) new JSONParser().parse(jObjAccountTarget.get("owed_from").toString());
                    for (int i = 0; i < jArrOwedFrom.size(); i++) {
                        JSONObject jObjOwedFrom = (JSONObject) jArrOwedFrom.get(i);
                        if (jObjOwedFrom.containsKey("target")) {
                            //searching if benef account has already on owed list
                            if (jObjOwedFrom.get("target").toString().equals(sessionActiveEntity.getActive())) {
                                foundTargetOwedFrom = true;
                                foundIndexOwedFrom = i;
                                balanceOwedFrom = Double.parseDouble(jObjOwedFrom.get("amount").toString());
                                break;
                            }
                        }
                    }
                    if (foundTargetOwedFrom) {
                        jArrOwedFrom.remove(foundIndexOwedFrom);
                    }
                    //add amount owed from already target owed list
                    JSONObject jObjOwedFrom = new JSONObject();
                    jObjOwedFrom.put("target", sessionActiveEntity.getActive());
                    jObjOwedFrom.put("amount", balanceOwedFrom + Math.abs(balanceSource));
                    jArrOwedFrom.add(jObjOwedFrom);
                    jObjAccountTarget = this.helper.addDataSession(target, "owed_from", jArrOwedFrom.toString());
//                }

                //add amount owed from already target owed list
                jObjOwedTo = new JSONObject();
                jObjOwedTo.put("target", target);
                jObjOwedTo.put("amount", balanceOwedTo + Math.abs(balanceSource));
                jArrOwedTo.add(jObjOwedTo);
                //update owed list
                jObjAccount = this.helper.addDataSession(sessionActiveEntity.getActive(), "owed_to", jArrOwedTo.toString());

                //update balance source amount with 0
                jObjAccount = this.helper.addDataSession(sessionActiveEntity.getActive(), "balance", "0");
                //System.out.println("acc source " + jObjAccount.toString());
                //System.out.println("acc bene " + jObjAccountTarget.toString());
            } else {
                //do normal case without owed amount
                jObjAccount = this.helper.addDataSession(sessionActiveEntity.getActive(), "balance", String.valueOf(balanceSource));
                balanceBeneficiary = Double.parseDouble(jObjAccountTarget.get("balance").toString());
                jObjAccountTarget = this.helper.addDataSession(target, "balance", String.valueOf(balanceBeneficiary + amount));
                //System.out.println("acc source " + jObjAccount.toString());
                //System.out.println("acc bene " + jObjAccountTarget.toString());
            }
            //System.out.println(jObjAccount.toString());
            //System.out.println(jObjAccountTarget.toString());
            jObjResp = jObjAccount;
            jObjResp.put("rc", "00");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return jObjResp;
    }
}
