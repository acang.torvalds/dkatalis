/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.implement;

import com.company.controller.ControllerSession;
import com.company.entity.SessionActiveEntity;
import com.company.interfc.InterfcDeposit;
import com.company.interfc.InterfcWithdraw;
import net.sf.ehcache.Cache;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * @author Hasan
 */
public class ImpleWithdraw implements InterfcWithdraw {
    private final Cache cache;
    private final ControllerSession controllerSession;

    public ImpleWithdraw(Cache cacheMaster) {
        this.cache = cacheMaster;
        this.controllerSession = new ControllerSession(cache);
    }

    @Override
    public JSONObject debitBalance(SessionActiveEntity sessionActiveEntity, String deposit) {
        JSONObject jObjResp = new JSONObject();
        try {
            //get data account
            JSONObject jObjSess = new JSONObject();
            jObjSess.put("sessId", sessionActiveEntity.getActive());
            jObjResp = this.controllerSession.getDataCookie(jObjSess);
            Double balance = Double.parseDouble(jObjResp.get("balance").toString());
            balance = balance - Double.parseDouble(deposit);
            if (balance < Double.parseDouble("0")) {
                jObjResp.put("rc", "01");
                jObjResp.put("message", "Insufficient balance!");
                return jObjResp;
            } else {
                //check if account has owed_to
                JSONArray jArrOwedTo = (JSONArray) new JSONParser().parse(jObjResp.get("owed_to").toString());
                boolean foundOwedTo = false;
                for (int i = 0; i < jArrOwedTo.size(); i++) {
                    JSONObject jObjOwedTo = (JSONObject) jArrOwedTo.get(i);
                    if (jObjOwedTo.containsKey("amount")) {
                        if (Double.parseDouble(jObjOwedTo.get("amount").toString()) > Double.parseDouble("0")) {
                            foundOwedTo = true;
                            break;
                        }
                    }
                }
                if (foundOwedTo) {
                    jObjResp.put("rc", "01");
                    jObjResp.put("message", "Cant withdraw if you has owed money, please deposit as much as you owed first!");
                    return jObjResp;
                }
                JSONObject jObjAccount = new JSONObject();
                jObjAccount.put("sessId", sessionActiveEntity.getActive());
                jObjAccount.put("key", "balance");
                jObjAccount.put("value", String.valueOf(balance));
                jObjResp = this.controllerSession.addDataCookie(jObjAccount);
                jObjResp.put("rc", "00");
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return jObjResp;
    }
}
