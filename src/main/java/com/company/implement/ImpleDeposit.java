/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.implement;

import com.company.controller.ControllerSession;
import com.company.entity.SessionActiveEntity;
import com.company.helper.Helper;
import com.company.interfc.InterfcDeposit;
import com.company.interfc.InterfcLogin;
import net.sf.ehcache.Cache;
import net.sf.ehcache.store.chm.SelectableConcurrentHashMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * @author Hasan
 */
public class ImpleDeposit implements InterfcDeposit {
    private final Cache cache;
    private final ControllerSession controllerSession;
    private final Helper helper;

    public ImpleDeposit(Cache cacheMaster) {
        this.cache = cacheMaster;
        this.controllerSession = new ControllerSession(cache);
        this.helper = new Helper(this.cache);
    }

    @Override
    public JSONObject addBalance(SessionActiveEntity sessionActiveEntity, String amount) {
        JSONObject jObjSess = new JSONObject();
        jObjSess.put("sessId", sessionActiveEntity.getActive());
        JSONObject jObjAccount = this.controllerSession.getDataCookie(jObjSess);
        Double balanceSourceBefore = Double.parseDouble(jObjAccount.get("balance").toString());
        Double amountDblBefore = Double.parseDouble(amount);
        Double amountDbl = Double.parseDouble(amount);
        try {
            //check has owed or not
            boolean foundOwedTo = false;
            JSONArray jArrOwedTo = (JSONArray) new JSONParser().parse(jObjAccount.get("owed_to").toString());
            JSONArray jArrOwedToCopy = (JSONArray) new JSONParser().parse(jArrOwedTo.toString());
            //account has owed amount list
            if (jArrOwedTo.size() > 1) {
                for (int i = 0; i < jArrOwedTo.size(); i++) {
                    JSONObject jObjOwedTo = (JSONObject) jArrOwedTo.get(i);
                    if (jObjOwedTo.containsKey("target")) {
                        //account have owed amount greater zero
                        if (Double.parseDouble(jObjOwedTo.get("amount").toString()) != Double.parseDouble("0")) {
                            //minus deposit amount with all owed amount
                            foundOwedTo = true;
                            String targetOwed = jObjOwedTo.get("target").toString();
                            //System.out.println("targetOwed " + targetOwed);
                            Double balanceOwed = Double.parseDouble(jObjOwedTo.get("amount").toString());
                            amountDbl = amountDbl - balanceOwed;

                            //if amount deposit not enough for pay owed amount
                            if (amountDbl <= Double.parseDouble("0")) {
                                JSONObject jObjOwedToNew = new JSONObject();
                                jObjOwedToNew.put("target", jObjOwedTo.get("target").toString());
                                jObjOwedToNew.put("amount", Math.abs(amountDbl));
                                jArrOwedToCopy.remove(i);
                                jArrOwedToCopy.add(jObjOwedToNew);
                                jObjAccount = this.helper.addDataSession(sessionActiveEntity.getActive(), "owed_to", jArrOwedToCopy.toString());
                                jObjAccount = this.helper.addDataSession(sessionActiveEntity.getActive(), "balance", String.valueOf(balanceSourceBefore));
                                //transfer amount to target owed account
                                JSONObject jObjAccountTarget = this.helper.getDataSession(targetOwed);
                                //System.out.println("target " + jObjAccountTarget.toString());
                                Double balanceTargetBefore = Double.parseDouble(jObjAccountTarget.get("balance").toString());
                                jObjAccountTarget = this.helper.addDataSession(targetOwed, "balance", String.valueOf(amountDblBefore + balanceTargetBefore));
                                System.out.println("Transferred $" + amountDblBefore + " to " + targetOwed);
                                //modify amount owed_from target
                                JSONArray jArrOwedFrom = (JSONArray) new JSONParser().parse(jObjAccountTarget.get("owed_from").toString());
                                JSONArray jArrOwedFromCopy = (JSONArray) new JSONParser().parse(jArrOwedFrom.toString());
                                for (int j = 0; j < jArrOwedFrom.size(); j++) {
                                    JSONObject jObjOwedFrom = (JSONObject) jArrOwedFrom.get(j);
                                    if (jObjOwedFrom.containsKey("target")) {
                                        if (jObjOwedFrom.get("target").toString().equals(sessionActiveEntity.getActive())) {
                                            Double amountOwedFrom = Double.parseDouble(jObjOwedFrom.get("amount").toString());
                                            JSONObject jObjOwedFromNew = new JSONObject();
                                            jObjOwedFromNew.put("target", sessionActiveEntity.getActive());
                                            jObjOwedFromNew.put("amount", amountOwedFrom - amountDblBefore);
                                            jArrOwedFromCopy.remove(j);
                                            jArrOwedFromCopy.add(jObjOwedFromNew);
                                            break;
                                        }
                                    }
                                }
                                jObjAccountTarget = this.helper.addDataSession(targetOwed, "owed_from", jArrOwedFromCopy.toString());
                                //System.out.println("data target : " + jObjAccountTarget.toString());
                                break;
                            } else {
                                //if amount deposit enough for pay owed amount
                                JSONObject jObjOwedNew = new JSONObject();
                                jObjOwedNew.put("target", jObjOwedTo.get("target").toString());
                                jObjOwedNew.put("amount", "0");
                                jArrOwedToCopy.remove(i);
                                jArrOwedToCopy.add(i, jObjOwedNew);
                                jObjAccount = this.helper.addDataSession(sessionActiveEntity.getActive(), "owed_to", jArrOwedToCopy.toString());
                                //transfer amount to target owed account
                                JSONObject jObjAccountTarget = this.helper.getDataSession(targetOwed);
                                Double balanceTargetBefore = Double.parseDouble(jObjAccountTarget.get("balance").toString());
                                jObjAccountTarget = this.helper.addDataSession(targetOwed, "balance", String.valueOf(balanceOwed + balanceTargetBefore));
                                System.out.println("Transferred $" + balanceOwed + " to " + targetOwed);
                                //reset new amount deposit after deduction
                                amountDblBefore = amountDblBefore - balanceOwed;
                                //modify amount owed_from target
                                JSONArray jArrOwedFrom = (JSONArray) new JSONParser().parse(jObjAccountTarget.get("owed_from").toString());
                                JSONArray jArrOwedFromCopy = (JSONArray) new JSONParser().parse(jArrOwedFrom.toString());
                                for (int j = 0; j < jArrOwedFrom.size(); j++) {
                                    JSONObject jObjOwedFrom = (JSONObject) jArrOwedFrom.get(j);
                                    if (jObjOwedFrom.containsKey("target")) {
                                        if (jObjOwedFrom.get("target").toString().equals(sessionActiveEntity.getActive())) {
                                            Double amountOwedFrom = Double.parseDouble(jObjOwedFrom.get("amount").toString());
                                            JSONObject jObjOwedFromNew = new JSONObject();
                                            jObjOwedFromNew.put("target", sessionActiveEntity.getActive());
                                            jObjOwedFromNew.put("amount", "0");
                                            jArrOwedFromCopy.remove(j);
                                            jArrOwedFromCopy.add(jObjOwedFromNew);
                                            break;
                                        }
                                    }
                                }
                                jObjAccountTarget = this.helper.addDataSession(targetOwed, "owed_from", jArrOwedFromCopy.toString());
                                //System.out.println(jObjAccountTarget.toString());
                            }
                        }
                    }
                }
                //if amount is greater than all owed amount and have at least one owed amount greater than zero
                if (amountDbl >= Double.parseDouble("0") && foundOwedTo) {
                    double balanceSourceNew = balanceSourceBefore + amountDbl;
                    jObjAccount = this.helper.addDataSession(sessionActiveEntity.getActive(), "balance", String.valueOf(balanceSourceNew));
                }
                //identify if account has owed data but all 0 amount owed
                if (!foundOwedTo) {
                    double balanceSourceNew = balanceSourceBefore + amountDbl;
                    jObjAccount = this.helper.addDataSession(sessionActiveEntity.getActive(), "balance", String.valueOf(balanceSourceNew));
                }
            } else {
                //account dont have owed amount even list of owed already payed
                double balanceSourceNew = balanceSourceBefore + amountDbl;
                jObjAccount = this.helper.addDataSession(sessionActiveEntity.getActive(), "balance", String.valueOf(balanceSourceNew));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //System.out.println("data sender " + jObjAccount.toString());
        return jObjAccount;
    }
}
