/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.implement;

import com.company.controller.ControllerSession;
import com.company.interfc.InterfcLogin;
import net.sf.ehcache.Cache;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * @author Hasan
 */
public class ImpleLogin implements InterfcLogin {
    private final Cache cache;
    private final ControllerSession controllerSession;

    public ImpleLogin(Cache cacheMaster) {
        this.cache = cacheMaster;
        this.controllerSession = new ControllerSession(cache);
    }

    @Override
    public JSONObject setCookie(String sessId) {
        JSONObject jObjDataSession = new JSONObject();
        try {
            //update user list
            JSONObject jObjUsrList = new JSONObject();
            jObjUsrList.put("sessId", "userList");
            jObjUsrList = this.getCookie(jObjUsrList);
            JSONArray jArrUserList = (JSONArray) new JSONParser().parse(jObjUsrList.get("userList").toString());
            jArrUserList.add(sessId);
            JSONObject jObjAccount = new JSONObject();
            jObjAccount.put("sessId", "userList");
            jObjAccount.put("key", "userList");
            jObjAccount.put("value", jArrUserList);
            jObjUsrList = this.controllerSession.addDataCookie(jObjAccount);
            //System.out.println(jObjUsrList.toString());

            //set session
            JSONObject jObjSession = new JSONObject();
            jObjSession.put("sessId", sessId);
            jObjDataSession = this.controllerSession.setCookie(jObjSession);

            //add new data username
            jObjAccount = new JSONObject();
            jObjAccount.put("sessId", sessId);
            jObjAccount.put("key", "username");
            jObjAccount.put("value", sessId);
            jObjDataSession = this.controllerSession.addDataCookie(jObjAccount);

            //add new data balance
            jObjAccount = new JSONObject();
            jObjAccount.put("sessId", sessId);
            jObjAccount.put("key", "balance");
            jObjAccount.put("value", "0");
            jObjDataSession = this.controllerSession.addDataCookie(jObjAccount);

            //add new data owed
            jObjAccount = new JSONObject();
            jObjAccount.put("sessId", sessId);
            jObjAccount.put("key", "owed_to");
            JSONArray jArrOwed = new JSONArray();
            JSONObject jObjOwed = new JSONObject();
            jArrOwed.add(jObjOwed);
            jObjAccount.put("value", jArrOwed);
            jObjDataSession = this.controllerSession.addDataCookie(jObjAccount);

            //add new data owed
            jObjAccount = new JSONObject();
            jObjAccount.put("sessId", sessId);
            jObjAccount.put("key", "owed_from");
            jArrOwed = new JSONArray();
            jObjOwed = new JSONObject();
            jArrOwed.add(jObjOwed);
            jObjAccount.put("value", jArrOwed);
            jObjDataSession = this.controllerSession.addDataCookie(jObjAccount);
        } catch (Exception ex){
            ex.printStackTrace();
        }

        return jObjDataSession;
    }

    @Override
    public JSONObject getCookie(JSONObject jSONObject) {
        JSONObject jObjDataSession = this.controllerSession.getDataCookie(jSONObject);
        return jObjDataSession;
    }
}
