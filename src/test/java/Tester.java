import com.company.controller.*;
import com.company.entity.SessionActiveEntity;
import junit.framework.Assert;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.skyscreamer.jsonassert.JSONAssert;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Tester {

    private Cache memoryOnlyCache;
    private SessionActiveEntity sessionActiveEntity;

    @Test
    @Order(1)
    public void allTest() {
        //--------------------------ehcache init for simulate database--------------------------------------
        // Already created a configuration object ...
        net.sf.ehcache.config.Configuration cacheConfiguration = new net.sf.ehcache.config.Configuration();
        //Create a CacheManager using custom configuration
        CacheManager cacheManager = CacheManager.create(cacheConfiguration);
        //--------------------------ehcache save the config entity------------------------
        this.memoryOnlyCache = new Cache("DKATALIS", 1000, false, true, 0, 0);
        cacheManager.addCache(memoryOnlyCache);
        //--------------------------simulate list of account-------------------------
        ControllerSession controllerSession = new ControllerSession(memoryOnlyCache);
        controllerSession.initData();

        //------create new session active entity-----------------------
        this.sessionActiveEntity = new SessionActiveEntity();
        sessionActiveEntity.setActive("allice");

        try {
            System.out.println("login allice tester, default balance 30");
            ControllerLogin controllerLogin = new ControllerLogin(memoryOnlyCache);
            JSONAssert.assertEquals("30", (String) controllerLogin.handleLogin("allice").get("balance"), false);
//            Assert.assertEquals(true, controllerLogin.handleLogin("allice"));

            System.out.println("deposit allice tester, depostit 20, last balance 50");
            ControllerDeposit controllerDeposit = new ControllerDeposit(memoryOnlyCache, sessionActiveEntity);
            JSONAssert.assertEquals("50.0", (String) controllerDeposit.handleDeposit("20").get("balance"), false);
//            Assert.assertEquals(true, controllerDeposit.handleDeposit("20"));

            System.out.println("withdraw allice tester, 20 last balance 30");
            ControllerWithdraw controllerWithdraw = new ControllerWithdraw(memoryOnlyCache, sessionActiveEntity);
            JSONAssert.assertEquals("30.0", (String) controllerWithdraw.handleWithdraw("20").get("balance"), false);

            System.out.println("transfer allice tester 20 to bob, last bal 10");
            String line = "transfer bob 20";
            String[] lineInputArr = line.split(" ");
            ControllerTransfer controllerTransfer = new ControllerTransfer(memoryOnlyCache, sessionActiveEntity);
            JSONAssert.assertEquals("10.0", (String) controllerTransfer.handleTransfer(lineInputArr).get("balance"), false);

            System.out.println("transfer allice tester 20 with owed 10 to bob, last bal 0");
            line = "transfer bob 20";
            lineInputArr = line.split(" ");
            controllerTransfer = new ControllerTransfer(memoryOnlyCache, sessionActiveEntity);
            JSONAssert.assertEquals("0", (String) controllerTransfer.handleTransfer(lineInputArr).get("balance"), false);

            System.out.println("login bob tester, bob last balance 40");
            sessionActiveEntity.setActive("bob");
            controllerLogin = new ControllerLogin(memoryOnlyCache);
            JSONAssert.assertEquals("30.0", (String) controllerLogin.handleLogin("bob").get("balance"), false);
//            Assert.assertEquals(true, controllerLogin.handleLogin("bob"));

            System.out.println("login allice tester, allice last balance 0");
            sessionActiveEntity.setActive("allice");
            controllerLogin = new ControllerLogin(memoryOnlyCache);
            JSONAssert.assertEquals("0", (String) controllerLogin.handleLogin("allice").get("balance"), false);

            System.out.println("deposit allice tester, depostit 20, last balance 10 cause owed 10 to bob");
            controllerDeposit = new ControllerDeposit(memoryOnlyCache, sessionActiveEntity);
            JSONAssert.assertEquals("10.0", (String) controllerDeposit.handleDeposit("20").get("balance"), false);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
