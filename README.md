this project is homework testing for DKatalis
this project is atm simulation, which have command
- login account, will create new account if there is no user exist
- deposit amount, simulate deposit balance account with some positive amount
- withdraw amount, simulate withdraw balance account with some positive amount
- transfer benefficiary amount, simulate transfer some amount to another account, create new account and deposit balance if beneficiary not exist, if some amount is more than balance sender balance, then balance will go to 0, and the lack will be become as owed amount which is will recalculated when deposit occur
- logout, to logout from system
- exit, exit program


compile with "mvn assembly:assembly"
run with "java -jar .\target\dkatalis-1.0-SNAPSHOT-jar-with-dependencies.jar"
or you can instant run on intellij IDE with "Application" configuration

dependency environtment as i create this project
- intellij IDE 2021.1 (Comunnity Edition)
- java sun jdk 8
- apache maven 3.8.3

dependency library as i create this project
- ehcache 2.10.6 (memory data management)
- json simple 1.1.1
- slf4j 1.6.6 for ehcache
- junit 5.5.2
- json assert 1.5.0 for junit testing
- maven-assembly-plugin 2.5.4 (for assembly in one jar)
- maven-compiler-plugin 3.1 (for compiler)

*you have need internet access to compile this project as maven project, i use pom.xml for tiny archieve, 
feel free if you need fat version as library include in lib project